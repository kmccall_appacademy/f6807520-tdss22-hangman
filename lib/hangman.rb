class Hangman
  attr_reader :guesser, :referee, :board, :guess_count



  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @board = []
    @guess_count = 10
  end

  def play
    while @guess_count > 0
    setup
    take_turn until over?
    conclude
    end
  end

  def setup
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = Array.new(length)
  end

  def take_turn
    hangman_man
    guess = @guesser.guess(@board)
    indices = @referee.check_guess(guess)
    @guess_count -= 1
    update_board(indices, guess)
    @guesser.handle_response(guess, indices)
  end

  private

  def hangman_man
    while @guess_count > 0
      if @guess_count == 10
        puts " |    ======="
        puts " |            "
        puts " |           "
        puts " |           "
        puts " |            "
        puts "  ==========="
        break

      elsif @guess_count == 9
        puts " |   |======="
        puts " |            "
        puts " |           "
        puts " |           "
        puts " |            "
        puts "  ==========="
        break
      elsif @guess_count == 8
        puts " |   +======="
        puts " |            "
        puts " |           "
        puts " |           "
        puts " |            "
        puts "  ==========="
        break
      elsif @guess_count == 7
        puts " |   +======="
        puts " |   |         "
        puts " |           "
        puts " |           "
        puts " |            "
        puts "  ==========="
        break
      elsif @guess_count == 6
        puts " |   +======="
        puts " |   |         "
        puts " |   O        "
        puts " |           "
        puts " |            "
        puts "  ==========="
        break
      elsif @guess_count == 5
        puts " |   +======="
        puts " |   |         "
        puts " |   O        "
        puts " |   |        "
        puts " |            "
        puts "  ==========="
        break
      elsif @guess_count == 4
        puts " |   +======="
        puts " |   |         "
        puts " |   O        "
        puts " | / |        "
        puts " |            "
        puts "  ==========="
        break
      elsif @guess_count == 3
        puts " |   +======="
        puts " |   |         "
        puts " |   O        "
        puts " | / |        "
        puts " |  /          "
        puts "  ==========="
        break
      elsif @guess_count == 2
        puts " |   +======="
        puts " |   |         "
        puts " |   O        "
        puts " | / | \       "
        puts " |  /          "
        puts "  ==========="
        break
      elsif @guess_count == 1
        puts " |   +======="
        puts " |   |         "
        puts " |   O        "
        puts " | / | \       "
        puts " |  / \         "
        puts "  ==========="
        break
      else @guess_count == 0
        puts " |   +======="
        puts " |   |       "
        puts " |   X       "
        puts " | / | \      "
        puts " |  / \     "
        puts "  ==========="
        break
      end
    end
  end

  def update_board(indices, guessed_letter)
    indices.each { |idx| @board[idx] = guessed_letter }
  end

  def over?
    @board.count(nil) == 0
  end

  def conclude
    puts "Well done, you guessed it! The word was #{@board.join("")}!!!"
  end

end

class HumanPlayer

  def initialize
    @guessed_ltrz = []

  end

  def register_secret_length(length)
    puts "The word you need to guess has a length of #{length} characters "
  end

  def guess(board)
    print_board(board)
    puts "Guess a letter:"
    print "> "
    guess = gets.chomp.downcase
    remember_guessed(guess)
  end

  def pick_secret_word
    puts "Think of a word! Enter its length: "
    print "> "
    gets.chomp.to_i
  end



  def check_guess(letter)
    puts "The computer guessed the letter '#{letter}'"
    puts "What positions does the letter occur at (ex. 1 3 5)"
    print "> "
    gets.chomp.split.map{ |ch| ch.to_i - 1}
  end

  def handle_response(guess, indices)
  end



  def remember_guessed(guess)

    unless @guessed_ltrz.include?(guess)
      @guessed_ltrz << guess
      return guess
    else
      puts "You already guessed #{guess}!"
      puts "You have already guessed: "
      p @guessed_ltrz

      puts "Please guess again"
      puts "Guess a letter:"
      print "> "
      guess = gets.chomp.downcase

      remember_guessed(guess)
    end

  end

  def print_board(board)

    board_s = board.map do |ch|
      ch.nil? ? "_" : ch
    end.join("")
    puts "Word you need to guess: #{board_s}"
  end
end

class ComputerPlayer
  def initialize(dictionary)
    @dictionary = dictionary
    @alphabet = ("a".."z").to_a
  end

  def candidate_words
    @dictionary
  end
  def self.read_dictionary
    File.readlines('lib/dictionary.txt').map(&:chomp)
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def check_guess(letter)
    indices = []
    @secret_word.each_char.with_index do |ch, idx|
      indices << idx if ch == letter
    end
    indices
  end

  def register_secret_length(length)
    #@secret_length = length

    @dictionary.select! { |word| word.length == length}

  end

  def handle_response(guess, indices)
    @dictionary.select! do |word|
      word_indices = []
      word.chars.each_with_index do |ch, idx|
        word_indices << idx if ch == guess
      end
      indices == word_indices

    end
    # indices.each { |idx| @known_parts[idx] = guess }
  end

  def guess(board)
    best_guess = @alphabet.first
    best_count = 0
    @alphabet.each do |ch|
      original_count = @dictionary.count { |word| word.include?(ch) }
      if original_count > best_count
        best_guess = ch
        best_count = original_count
      end
    end
    @alphabet.delete(best_guess)
    best_guess
  end




end


if __FILE__ == $PROGRAM_NAME
  dictionary = ComputerPlayer.read_dictionary
  players = {
    referee: HumanPlayer.new,
    guesser: ComputerPlayer.new(dictionary)
  }
  game = Hangman.new(players)
  game.play
end
